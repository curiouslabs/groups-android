package io.curiouslabs.groups.model.domain;

import io.curiouslabs.groups.model.dto.GroupDTO;

/**
 * Created by visitor15 on 11/24/16.
 */

public class Group {

    public final String id;
    public final Long creationDate;
    public final Long lastUpdated;
    public final Location location;
    public final String visibility;
    public final String title;
    public final String description;
//    public final Double distance;

    public Group(String id,
                 Long creationDate,
                 Long lastUpdated,
                 Location location,
                 String visibility,
                 String title,
                 String description) {
        this.id             = id;
        this.creationDate   = creationDate;
        this.lastUpdated    = lastUpdated;
        this.location       = location;
        this.visibility     = visibility;
        this.title          = title;
        this.description    = description;
//        this.distance       = distance;
    }

    public static Group fromDTO(final GroupDTO groupDTO) {
        return new Group(groupDTO.id,
                groupDTO.creationDate,
                groupDTO.lastUpdated,
                Location.fromDTO(groupDTO.location),
                groupDTO.visibility,
                groupDTO.title,
                groupDTO.description);
    }
}
