package io.curiouslabs.groups.model.domain;

import io.curiouslabs.groups.model.dto.LocationDTO;

/**
 * Created by visitor15 on 11/25/16.
 */

public class Location {

    public final Double latitude;
    public final Double longitude;

    public Location(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static final Location fromDTO(final LocationDTO locationDTO) {
        return new Location(locationDTO.latitude, locationDTO.longitude);
    }
}
