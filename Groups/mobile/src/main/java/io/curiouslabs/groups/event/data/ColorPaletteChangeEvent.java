package io.curiouslabs.groups.event.data;

/**
 * Created by visitor15 on 12/30/16.
 */

public class ColorPaletteChangeEvent {

    private final int vibrantColor;
    private final int darkVibrantColor;

    public ColorPaletteChangeEvent(int vibrantColor, int darkVibrantColor) {
        this.vibrantColor = vibrantColor;
        this.darkVibrantColor = darkVibrantColor;
    }

    public int getVibrantColor() {
        return this.vibrantColor;
    }

    public int getDarkVibrantColor() {
        return this.darkVibrantColor;
    }
}
