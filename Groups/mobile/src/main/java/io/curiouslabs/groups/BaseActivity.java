package io.curiouslabs.groups;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.event.PermissionGrantedEvent;
import io.curiouslabs.groups.event.data.OnActivityResultEvent;
import io.curiouslabs.groups.event.ui.NavDrawerEvent;
import io.curiouslabs.groups.event.ui.ShowFragmentRequestEvent;
import io.curiouslabs.groups.gui.DefaultFabOnClickListener;
import io.curiouslabs.groups.gui.DefaultNavDrawerEventListener;
import io.curiouslabs.groups.gui.DefaultNavDrawerListener;
import io.curiouslabs.groups.location.LocationProvider;
import io.curiouslabs.groups.service.BackendService;

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract View onSimpleCreateView(View parent, String name, Context context, AttributeSet attrs);

    protected abstract void onSimpleResume();

    protected abstract void onSimplePause();

    protected abstract void onSimpleStart();

    protected abstract void onSimpleOptionsItemSelected(MenuItem item);

    protected abstract void onSimpleCreateOptionsMenu(Menu menu);

    protected abstract void onSimpleBackPressed();

    protected abstract void onSimpleCreate(Bundle savedInstanceState);

    @Inject
    DefaultNavDrawerListener defaultNavDrawerListener;

    @Inject
    DefaultFabOnClickListener defaultFabOnClickListener;

    @Inject @Named("defaultBus")
    protected EventBus eventBus;

    @Inject
    public BackendService backendService;

    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    protected NavigationView navigationView;

//    @BindView(R.id.toolbar)
//    protected Toolbar toolbar;

//    @BindView(R.id.fab)
//    protected FloatingActionButton fab;

    @BindView(R.id.root_container)
    protected RelativeLayout rootContainer;

    private ActionBarDrawerToggle toggle;

    private DefaultNavDrawerEventListener defaultNavDrawerEventListener;

    private OnActivityResultEvent onActivityResultEvent = null;

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Logger.d("GOT PERMISSION REQUEST CODE " + requestCode);
        switch (requestCode) {
            case LocationProvider.PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Logger.d("GOT PERMISSION!");
                    EventBus.getDefault().post(new PermissionGrantedEvent(LocationProvider.PERMISSION_REQUEST_CODE));
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Logger.e("ERROR REQUESTING PERMISSION!");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        bindResourcesInternal();
        bindResources();
        defaultNavDrawerEventListener = new DefaultNavDrawerEventListener(drawerLayout);
//        fab.setOnClickListener(defaultFabOnClickListener);
//        setSupportActionBar(toolbar);
//        setListeners();
        onSimpleCreate(savedInstanceState);
    }

    protected abstract void bindResources();

    protected abstract int getContentView();

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        onSimpleBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        onSimpleCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        onSimpleOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventBus.register(defaultNavDrawerEventListener);
        eventBus.register(this);
        navigationView.setNavigationItemSelectedListener(defaultNavDrawerListener);
        onSimpleResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        eventBus.unregister(defaultNavDrawerEventListener);
        eventBus.unregister(this);
        onSimplePause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        onSimpleStart();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                    if(onActivityResultEvent != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Logger.d("OnActivityResultEvent POSTING!");
                                eventBus.post(onActivityResultEvent);
                                onActivityResultEvent = null;
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(onSimpleCreateView(parent, name, context, attrs), name, context, attrs);
    }

    private void bindResourcesInternal() {
        GroupsApp.getContext().getSimpleActivityComponent().inject(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("GOT ACTIVITY RESULT FOR REQUEST CODE " + requestCode);

        if(resultCode == Activity.RESULT_OK) {
            Bundle b = new Bundle();
            b.putString("uri", data.getDataString());
            onActivityResultEvent = new OnActivityResultEvent(resultCode, b);
        }
        else {
            Logger.e("ERROR onActivityResult");
            onActivityResultEvent = null;
        }
    }

    public void initializeToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        setListeners(toolbar);
    }

    public void setToolbarTitleText(final String titleText) {
        getSupportActionBar().setTitle(titleText);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(NavDrawerEvent event) {
        Logger.d("NAVIGATION DRAW EVENT RECEIVED.");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(ShowFragmentRequestEvent event) {
        Class<? extends Fragment> fragmentClass = event.getFragmentClass();
        Fragment fragment = null;
        try {
            fragment = fragmentClass.newInstance();
            fragment.setArguments(event.getData());
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragTransaction = fragmentManager.beginTransaction();
            fragTransaction.replace(R.id.root_container, fragment);
            fragTransaction.addToBackStack(null);
            fragTransaction.commit();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void setListeners(Toolbar toolbar) {
//        defaultNavDrawerEventListener = new DefaultNavDrawerEventListener(drawerLayout);
        toggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
//        fab.setOnClickListener(defaultFabOnClickListener);
    }
}
