package io.curiouslabs.groups.common;

import android.location.Location;

import org.greenrobot.eventbus.EventBus;

import io.curiouslabs.groups.event.data.LocationChangedEvent;

/**
 * Created by visitor15 on 12/29/16.
 */

public class DefaultLocationCallback implements GroupsCallback<Location, GroupsError> {

    @Override
    public void onSuccess(Location location) {
        EventBus.getDefault().post(new LocationChangedEvent(location));
    }

    @Override
    public void onFailure(GroupsError groupsError) {

    }
}
