package io.curiouslabs.groups.model.persistant;

import io.curiouslabs.groups.model.domain.UserColorPalette;
import io.realm.RealmObject;

/**
 * Created by visitor15 on 12/31/16.
 */

public class PersistedUserColorPalette extends RealmObject {

    private int vibrantColor;
    private int darkVibrantColor;
    private int mutedVibrant;
    private int mutedDarkVibrant;

    public PersistedUserColorPalette() {

    }

    public PersistedUserColorPalette(final UserColorPalette userColorPalette) {
        this(userColorPalette.getVibrantColor(), userColorPalette.getDarkVibrantColor(), userColorPalette.getMutedVibrant(), userColorPalette.getMutedDarkVibrant());
    }

    public PersistedUserColorPalette(int vibrantColor, int darkVibrantColor, int mutedVibrant, int mutedDarkVibrant) {
        this.vibrantColor       = vibrantColor;
        this.darkVibrantColor   = darkVibrantColor;
        this.mutedVibrant       = mutedVibrant;
        this.mutedDarkVibrant   = mutedDarkVibrant;
    }

    public int getVibrantColor() {
        return vibrantColor;
    }

    public int getDarkVibrantColor() {
        return darkVibrantColor;
    }

    public int getMutedVibrant() {
        return mutedVibrant;
    }

    public int getMutedDarkVibrant() {
        return mutedDarkVibrant;
    }

    public static final UserColorPalette toDomainModel(final PersistedUserColorPalette persistedUserColorPalette) {
        return new UserColorPalette(persistedUserColorPalette.getVibrantColor(), persistedUserColorPalette.getDarkVibrantColor(), persistedUserColorPalette.getMutedVibrant(), persistedUserColorPalette.getMutedDarkVibrant());
    }
}
