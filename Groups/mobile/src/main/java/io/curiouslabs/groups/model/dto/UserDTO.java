package io.curiouslabs.groups.model.dto;

/**
 * Created by visitor15 on 11/24/16.
 */

public class UserDTO extends JsonEntity<UserDTO> {

    public final String id;
    public final String email;
    public final String firstName;
    public final String lastName;
    public final String displayName;

    public UserDTO(String id, String email, String firstName, String lastName, String displayName) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.displayName = displayName;
    }

    @Override
    protected Class<UserDTO> getClassInternal() {
        return UserDTO.class;
    }
}
