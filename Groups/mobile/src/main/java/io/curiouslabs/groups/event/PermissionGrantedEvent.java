package io.curiouslabs.groups.event;

/**
 * Created by visitor15 on 12/29/16.
 */

public class PermissionGrantedEvent {

    private final int permissionRequestCode;

    public PermissionGrantedEvent(final int permissionRequestCode) {
        this.permissionRequestCode = permissionRequestCode;
    }

    public int getPermissionRequestCode() {
        return this.permissionRequestCode;
    }
}
