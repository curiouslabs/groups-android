package io.curiouslabs.groups.common;

/**
 * Created by visitor15 on 11/25/16.
 */

public interface GroupsCallback<T, E extends GroupsError> {
    public void onSuccess(T t);

    public void onFailure(E e);
}
