package io.curiouslabs.groups.dagger;

import dagger.Component;
import io.curiouslabs.groups.activity.LoginActivity;
import io.curiouslabs.groups.dagger.module.BackendServiceModule;
import io.curiouslabs.groups.dagger.module.RegistrationServiceModule;
import io.curiouslabs.groups.dagger.module.UserOnboardingServiceModule;
import io.curiouslabs.groups.fragment.LoginFragment;
import io.curiouslabs.groups.fragment.NearByGroupsFragment;
import io.curiouslabs.groups.fragment.RegisterUserFragment;

/**
 * Created by visitor15 on 11/28/16.
 */
@Component(modules = { BackendServiceModule.class, UserOnboardingServiceModule.class, RegistrationServiceModule.class })
public interface LoginActivityComponent {

    void inject(LoginActivity loginActivity);

    void inject(RegisterUserFragment registerUserFragment);

    void inject(LoginFragment loginFragment);

    void inject(NearByGroupsFragment nearByGroupsFragment);
}
