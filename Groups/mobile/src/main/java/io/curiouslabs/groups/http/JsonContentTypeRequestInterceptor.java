package io.curiouslabs.groups.http;

import com.orhanobut.logger.Logger;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by visitor15 on 11/25/16.
 */

public class JsonContentTypeRequestInterceptor implements Interceptor {

    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Logger.d("Appending content type - application/json");
        request = request.newBuilder().addHeader("Content-Type", "application/json").build();
        Response response = chain.proceed(request);
        return response;
    }
}