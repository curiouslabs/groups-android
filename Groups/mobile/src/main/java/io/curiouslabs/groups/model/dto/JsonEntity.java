package io.curiouslabs.groups.model.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by visitor15 on 11/25/16.
 */

public abstract class JsonEntity<T> {

//    private GsonBuilder builder = new GsonBuilder();
//    private Gson gson           = builder.create();

    protected abstract Class<T> getClassInternal();

    public String toJson() {
        GsonBuilder builder = new GsonBuilder();
        Gson gson           = builder.create();
        return gson.toJson(this);
    }

    public T fromJson(final String jsonStr) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson           = builder.create();
        return gson.fromJson(jsonStr, getClassInternal());
    }
}
