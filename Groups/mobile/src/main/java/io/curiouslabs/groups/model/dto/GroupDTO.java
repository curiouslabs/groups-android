package io.curiouslabs.groups.model.dto;

/**
 * Created by visitor15 on 11/24/16.
 */

public class GroupDTO extends JsonEntity<GroupDTO> {

    public final String id;
    public final Long creationDate;
    public final Long lastUpdated;
    public final LocationDTO location;
    public final String visibility;
    public final String title;
    public final String description;
//    public final Double distance;

    public GroupDTO(String id,
                    Long creationDate,
                    Long lastUpdated,
                    LocationDTO location,
                    String visibility,
                    String title,
                    String description) {
        this.id = id;
        this.creationDate = creationDate;
        this.lastUpdated = lastUpdated;
        this.location = location;
        this.visibility = visibility;
        this.title = title;
        this.description = description;
//        this.distance = distance;
    }

    @Override
    protected Class<GroupDTO> getClassInternal() {
        return GroupDTO.class;
    }
}
