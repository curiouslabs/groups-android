package io.curiouslabs.groups.dagger;

import dagger.Component;
import io.curiouslabs.groups.dagger.module.BackendServiceModule;
import io.curiouslabs.groups.fragment.DetailedGroupFragment;

/**
 * Created by visitor15 on 11/25/16.
 */
@Component(modules = { BackendServiceModule.class })
public interface BackendServiceComponent {

    void inject(Object object);

    void inject(DetailedGroupFragment detailedGroupFragment);
}
