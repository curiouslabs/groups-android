package io.curiouslabs.groups.model.persistant;

import io.curiouslabs.groups.model.domain.User;
import io.curiouslabs.groups.model.domain.UserColorPalette;
import io.curiouslabs.groups.model.domain.UserProfile;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by visitor15 on 12/31/16.
 */

public class PersistedUserProfile extends RealmObject {

    @PrimaryKey
    private String id;

    private String profilePicUri;

    private PersistedUserColorPalette persistedUserColorPalette;

    private PersistedUser persistedUser;

    public PersistedUserProfile() {

    }

    public PersistedUserProfile(PersistedUser persistedUser, String profilePicUri, PersistedUserColorPalette persistedUserColorPalette) {
        this.id                         = persistedUser.getId();
        this.persistedUser              = persistedUser;
        this.profilePicUri              = profilePicUri;
        this.persistedUserColorPalette  = persistedUserColorPalette;
    }

    public String getId() {
        return id;
    }

    public String getProfilePicUri() {
        return profilePicUri;
    }

    public PersistedUserColorPalette getPersistedUserColorPalette() {
        return persistedUserColorPalette;
    }

    public PersistedUser getPersistedUser() {
        return persistedUser;
    }

    public static UserProfile toDomainModel(final PersistedUserProfile persistedUserProfile) {
        User user                           = PersistedUser.toDomainModel(persistedUserProfile.getPersistedUser());
        UserColorPalette userColorPalette   = PersistedUserColorPalette.toDomainModel(persistedUserProfile.getPersistedUserColorPalette());
        String profilePicUri                = persistedUserProfile.getProfilePicUri();
        return new UserProfile(user, profilePicUri, userColorPalette);
    }
}
