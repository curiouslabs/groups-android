package io.curiouslabs.groups.dagger.provider;

/**
 * Created by visitor15 on 11/25/16.
 */

public interface AuthProvider {

    public String getToken();

    public void setToken(final String newToken);
}
