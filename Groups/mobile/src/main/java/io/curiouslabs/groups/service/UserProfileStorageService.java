package io.curiouslabs.groups.service;

import com.google.common.base.Optional;

import io.curiouslabs.groups.model.domain.UserProfile;
import io.curiouslabs.groups.model.persistant.PersistedUser;
import io.curiouslabs.groups.model.persistant.PersistedUserColorPalette;
import io.curiouslabs.groups.model.persistant.PersistedUserProfile;
import io.realm.Realm;

/**
 * Created by visitor15 on 1/2/17.
 */

public class UserProfileStorageService {

    public Optional<UserProfile> findUserProfileById(final String id) {
        Realm instance = Realm.getDefaultInstance();
        Optional<PersistedUserProfile> optResult = Optional.fromNullable(instance.where(PersistedUserProfile.class).equalTo("id", id).findFirst());
        instance.close();

        if(optResult.isPresent()) {
            return Optional.of(PersistedUserProfile.toDomainModel(optResult.get()));
        }
        else {
            return Optional.absent();
        }
    }

    public void saveUserProfile(final UserProfile userProfile) {
        PersistedUser persistedUser                         = new PersistedUser(userProfile.getUser());
        PersistedUserColorPalette persistedUserColorPalette = new PersistedUserColorPalette(userProfile.getUserColorPalette());
        PersistedUserProfile persistedUserProfile           = new PersistedUserProfile(persistedUser, userProfile.getProfilePicUri(), persistedUserColorPalette);

        Realm instance = Realm.getDefaultInstance();
        instance.beginTransaction();
        instance.copyToRealmOrUpdate(persistedUserProfile);
        instance.commitTransaction();
        instance.close();
    }
}
