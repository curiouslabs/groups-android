package io.curiouslabs.groups.gui;

import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.view.MenuItem;

import org.greenrobot.eventbus.EventBus;

import io.curiouslabs.groups.R;
import io.curiouslabs.groups.event.ui.NavDrawerEvent;

/**
 * Created by visitor15 on 10/22/16.
 */

public class DefaultNavDrawerListener implements OnNavigationItemSelectedListener {

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        EventBus.getDefault().post(new NavDrawerEvent(NavDrawerEvent.Action.ITEM_CLICKED, id));

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
//        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
