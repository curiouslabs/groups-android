package io.curiouslabs.groups.model.dto;

import io.curiouslabs.groups.common.GroupsError;

/**
 * Created by visitor15 on 11/25/16.
 */

public class BackendError implements GroupsError {

    private final int code;
    private final String message;

    public BackendError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
