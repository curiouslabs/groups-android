package io.curiouslabs.groups.event;

/**
 * Created by visitor15 on 12/28/16.
 */

public interface Eventable<T> {

    public EventType getEventType();
}
