package io.curiouslabs.groups.http.builder;

import io.curiouslabs.groups.http.AuthenticatedRequestInterceptor;
import io.curiouslabs.groups.http.BasicAuthenticatedRequestInterceptor;
import io.curiouslabs.groups.http.JsonContentTypeRequestInterceptor;
import io.curiouslabs.groups.http.api.Backend;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by visitor15 on 11/25/16.
 */

public class BackendBuilder {

    private static BackendBuilder instance;

    private BackendBuilder() {
        instance = this;
    }

    public static BackendBuilder getInstance() {
        if (instance == null) {
            new BackendBuilder();
        }
        return instance;
    }

    public Backend buildAuthenticatedBackend() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new AuthenticatedRequestInterceptor())
                .addInterceptor(new JsonContentTypeRequestInterceptor())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.8:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(Backend.class);
    }

    public Backend buildBasicAuthenticatedBackend() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthenticatedRequestInterceptor())
                .addInterceptor(new JsonContentTypeRequestInterceptor())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.8:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(Backend.class);
    }

    public Backend buildUnauthenticatedBackend() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new JsonContentTypeRequestInterceptor())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.8:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(Backend.class);
    }
}
