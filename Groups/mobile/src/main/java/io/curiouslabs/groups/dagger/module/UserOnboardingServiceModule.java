package io.curiouslabs.groups.dagger.module;

import dagger.Module;
import dagger.Provides;
import io.curiouslabs.groups.service.UserOnboardingService;

/**
 * Created by visitor15 on 1/2/17.
 */
@Module
public class UserOnboardingServiceModule {

    @Provides
    public static UserOnboardingService provideUserOnboardingService() {
        return new UserOnboardingService();
    }
}
