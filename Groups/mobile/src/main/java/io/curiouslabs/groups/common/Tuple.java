package io.curiouslabs.groups.common;

/**
 * Created by visitor15 on 12/29/16.
 */

public class Tuple<A, B> {

    private final A a;
    private final B b;

    public Tuple(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public A getA() {
        return this.a;
    }

    public B getB() {
        return this.b;
    }
}
