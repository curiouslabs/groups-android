package io.curiouslabs.groups.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Strings;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.BaseActivity;
import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.common.GroupsCallback;
import io.curiouslabs.groups.model.domain.Group;
import io.curiouslabs.groups.model.dto.BackendError;
import io.curiouslabs.groups.service.BackendService;

/**
 * Created by visitor15 on 1/3/17.
 */

public class DetailedGroupFragment extends Fragment {

    @Inject
    BackendService backendService;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    private DetailedGroupHeaderFragment detailedGroupHeaderFragment;
    private GroupInformationFragment groupInformationFragment;

    private String groupId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detailed_group_fragment_layout, container, false);
        GroupsApp.getContext().getBackendServiceComponent().inject(this);
        ButterKnife.bind(this, v);

        ((BaseActivity) getActivity()).initializeToolbar(toolbar);

        detailedGroupHeaderFragment = (DetailedGroupHeaderFragment) getChildFragmentManager().findFragmentById(R.id.group_header_fragment);
        groupInformationFragment = (GroupInformationFragment) getChildFragmentManager().findFragmentById(R.id.group_information_fragment);

        Bundle arguments = getArguments() != null ? getArguments() : savedInstanceState;
        if(arguments != null) {
            groupId = arguments.getString("groupId", "");
            if(!Strings.isNullOrEmpty(groupId)) {
                backendService.getGroup(groupId, new GroupsCallback<Group, BackendError>() {
                    @Override
                    public void onSuccess(Group group) {
                        updateGroup(group);
                    }

                    @Override
                    public void onFailure(BackendError backendError) {
                        // TODO - show error to user
                    }
                });
            }
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("groupId", groupId);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void updateGroup(final Group group) {
        ((BaseActivity) getActivity()).setToolbarTitleText(group.title);
        detailedGroupHeaderFragment.setGroup(group);
        groupInformationFragment.setGroup(group);
    }
}
