package io.curiouslabs.groups.http.api;

import java.util.List;

import io.curiouslabs.groups.model.dto.AuthenticationReceiptDTO;
import io.curiouslabs.groups.model.dto.AuthenticationRequestDTO;
import io.curiouslabs.groups.model.dto.GroupDTO;
import io.curiouslabs.groups.model.dto.UserCreateRequestDTO;
import io.curiouslabs.groups.model.dto.UserDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by visitor15 on 11/25/16.
 */

public interface Backend {

    @POST("groups/authenticate")
    Call<AuthenticationReceiptDTO> authenticateUser(@Body AuthenticationRequestDTO authenticationRequestDTO);

    @PUT("user")
    Call<UserDTO> createUser(@Body UserCreateRequestDTO userCreateRequestDTO);

    @GET("user/{id}")
    Call<UserDTO> getUser(@Path("id") String userId);

    @GET("user/{id}/groups")
    Call<List<GroupDTO>> getGroupsForUser(@Path("id") String userId);

    @GET("group/{id}")
    Call<GroupDTO> getGroup(@Path("id") String groupId);

    @GET("group/search")
    Call<List<GroupDTO>> searchGroups(@Query("latitude") double latitude, @Query("longitude") double longitude);
}
