package io.curiouslabs.groups.dagger.module;

import dagger.Module;
import dagger.Provides;
import io.curiouslabs.groups.location.LocationProvider;

/**
 * Created by visitor15 on 12/29/16.
 */
@Module
public class LocationModule {

    @Provides
    public static LocationProvider provideLocationProvider() {
        return new LocationProvider();
    }
}
