package io.curiouslabs.groups.model.dto;

/**
 * Created by visitor15 on 11/25/16.
 */

public class AuthenticationReceiptDTO extends JsonEntity<AuthenticationReceiptDTO> {

    public final String userId;
    public final String accessToken;
    public final String tokenType;
    public final Long expiresIn;
    public final String refreshToken;

    public AuthenticationReceiptDTO(String userId,
                                    String accessToken,
                                    String tokenType,
                                    Long expiresIn,
                                    String refreshToken) {
        this.userId         = userId;
        this.accessToken    = accessToken;
        this.tokenType      = tokenType;
        this.expiresIn      = expiresIn;
        this.refreshToken   = refreshToken;
    }

    @Override
    protected Class getClassInternal() {
        return AuthenticationReceiptDTO.class;
    }
}
