package io.curiouslabs.groups.common;

/**
 * Created by visitor15 on 11/25/16.
 */

public interface ErrorFactory<T> {

    public T create(final int code, final String message);
}
