package io.curiouslabs.groups.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.common.GroupsCallback;
import io.curiouslabs.groups.event.data.ColorPaletteChangeEvent;
import io.curiouslabs.groups.event.ui.ShowFragmentRequestEvent;
import io.curiouslabs.groups.gui.RecyclerViewGridSpacer;
import io.curiouslabs.groups.model.domain.Group;
import io.curiouslabs.groups.model.dto.BackendError;
import io.curiouslabs.groups.service.BackendService;
import io.curiouslabs.groups.util.PixelConverter;

/**
 * Created by visitor15 on 12/12/16.
 */

public class NearByGroupsFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.loading_view)
    FrameLayout loadingView;

    @Inject
    BackendService backendService;

    private int personalizedColor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.nearby_groups_fragment_layout, container, false);
        GroupsApp.getContext().getLoginActivityComponent().inject(this);
        ButterKnife.bind(this, v);
        init();
        recyclerView.addItemDecoration(new RecyclerViewGridSpacer((int) PixelConverter.convertDpToPixel(8, getActivity()), (int) PixelConverter.convertDpToPixel(8, getActivity()), 2, true));
        populateNearByGroups(personalizedColor);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("personalized_color", personalizedColor);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(ColorPaletteChangeEvent event) {
        Logger.d("GOT ColorPaletteChangeEvent!");
        this.personalizedColor = event.getDarkVibrantColor();
        if(recyclerView.getAdapter() != null) {
            ((GroupsAdapter) recyclerView.getAdapter()).setPersonalizedColor(this.personalizedColor);
            recyclerView.clearFocus();
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    private void init() {
        personalizedColor = GroupsApp.getContext().getCurrentUserProfile().get().getUserColorPalette().getDarkVibrantColor();
    }

    private void populateNearByGroups(final int personalizedColor) {
        Logger.d("Getting nearby groups");
        backendService.getNearByGroups(getActivity(), new GroupsCallback<List<Group>, BackendError>() {
            @Override
            public void onSuccess(List<Group> groups) {
                loadingView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setAdapter(new GroupsAdapter(GroupsApp.getContext(), groups, personalizedColor));
                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            }

            @Override
            public void onFailure(BackendError backendError) {
                Logger.d("FAILED TO GET NEARBY GROUPS %s", backendError.getMessage());
            }
        });
    }

    public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.ViewHolder> {

        private int personalizedColor;

        Bitmap image;

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView titleTextView;
            public TextView descriptionTextView;
            public ImageView groupPic;

            public ViewHolder(View itemView) {
                super(itemView);
                titleTextView       = (TextView) itemView.findViewById(R.id.title);
                descriptionTextView = (TextView) itemView.findViewById(R.id.description);
                groupPic            = (ImageView) itemView.findViewById(R.id.group_pic);
            }
        }

        private List<Group> groups;
        private Context c;

        // Pass in the contact array into the constructor
        public GroupsAdapter(Context context, List<Group> groups, int personalizedColor) {
            this.groups = groups;
            c = context;
            this.personalizedColor = personalizedColor;
            image = Bitmap.createBitmap(PixelConverter.convertDpToPixel(54f, c), PixelConverter.convertDpToPixel(54f, c), Bitmap.Config.ARGB_8888);
            image.eraseColor(personalizedColor);
        }

        private Context getContext() {
            return c;
        }

        public void setPersonalizedColor(final int personalizedColor) {
            this.personalizedColor = personalizedColor;
            image.eraseColor(personalizedColor);
        }

        @Override
        public GroupsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            Logger.d("LALALA");
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);

            // Inflate the custom layout
            View contactView = inflater.inflate(R.layout.group_item_layout_default, parent, false);

//            Transition transition = new Slide(Gravity.RIGHT);
//            transition.setDuration(500);
//            transition.setInterpolator(new FastOutSlowInInterpolator());
//            transition.setStartDelay(200);
//            TransitionManager.beginDelayedTransition(parent, transition);

            // Return a new holder instance
            ViewHolder viewHolder = new ViewHolder(contactView);
            return viewHolder;
        }

        // Involves populating data into the item through holder
        @Override
        public void onBindViewHolder(GroupsAdapter.ViewHolder viewHolder, int position) {
            // Get the data model based on position
            final Group group = groups.get(position);

//            Logger.d("GOT GROUP %s", group.title);

            viewHolder.titleTextView.setText(group.title);
            viewHolder.descriptionTextView.setText(group.description);
            viewHolder.groupPic.setBackgroundColor(personalizedColor);
//            viewHolder.groupPic.setImageBitmap(image);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Logger.d("GOT GROUP ITEM CLICK : " + group.id);
                    Bundle b = new Bundle();
                    b.putString("groupId", group.id);
                    EventBus.getDefault().post(new ShowFragmentRequestEvent(DetailedGroupFragment.class, b));
                }
            });
        }

        // Returns the total count of items in the list
        @Override
        public int getItemCount() {
            return groups.size();
        }
    }

}