package io.curiouslabs.groups.model.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by visitor15 on 11/28/16.
 */

public class UserAuthenticationRequest extends RealmObject {

    public static final String KEY = "user_request";

    @PrimaryKey
    private String id;
    private String email;
    private String password;
    private String accessToken;
    private String refreshToken;

    public UserAuthenticationRequest() {
        this.id = "";
        this.email = "";
        this.password = "";
        this.accessToken = "";
        this.refreshToken = "";
    }

    public UserAuthenticationRequest(String id, String email, String password, String accessToken, String refreshToken) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }
}
