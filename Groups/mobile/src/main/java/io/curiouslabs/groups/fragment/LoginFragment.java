package io.curiouslabs.groups.fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.common.base.Strings;
import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.activity.UserActivity;
import io.curiouslabs.groups.common.GroupsCallback;
import io.curiouslabs.groups.model.domain.AuthenticationReceipt;
import io.curiouslabs.groups.model.domain.User;
import io.curiouslabs.groups.model.domain.UserAuthenticationRequest;
import io.curiouslabs.groups.model.domain.UserColorPalette;
import io.curiouslabs.groups.model.domain.UserProfile;
import io.curiouslabs.groups.model.dto.AuthenticationRequestDTO;
import io.curiouslabs.groups.model.dto.BackendError;
import io.curiouslabs.groups.service.BackendService;
import io.curiouslabs.groups.service.UserOnboardingService;
import io.realm.Realm;

/**
 * Created by visitor15 on 11/29/16.
 */

public class LoginFragment extends Fragment {

    @Inject
    BackendService backendService;

    @Inject
    UserOnboardingService userOnboardingService;

    @BindView(R.id.input_email)
    EditText inputEmail;

    @BindView(R.id.input_password)
    EditText inputPassword;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.login_layout, container, false);
        GroupsApp.getContext().getLoginActivityComponent().inject(this);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        handleLoggedInState();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailStr = inputEmail.getText().toString();
                String passwordStr = inputPassword.getText().toString();

                if(!Strings.isNullOrEmpty(emailStr) && !Strings.isNullOrEmpty(passwordStr) && emailStr.contains("@")) {
                    backendService.authenticate(new AuthenticationRequestDTO(Base64.encodeToString(new String(emailStr + ":" + passwordStr).getBytes(), Base64.URL_SAFE), emailStr, passwordStr, ""), new GroupsCallback<AuthenticationReceipt, BackendError>() {
                        @Override
                        public void onSuccess(final AuthenticationReceipt authenticationReceipt) {
                            Logger.d("Got authentication for user %s. Access token: %s", authenticationReceipt.userId, authenticationReceipt.accessToken);
//                            final Realm instance = Realm.getDefaultInstance();
//                            UserAuthenticationRequest result = instance.where(UserAuthenticationRequest.class).equalTo("id", UserAuthenticationRequest.KEY).findFirst();
//                            instance.beginTransaction();
//                            result.setAccessToken(authenticationReceipt.accessToken);
//                            instance.copyToRealmOrUpdate(result);
//                            instance.commitTransaction();
//                            instance.close();

                            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {

                                @Override
                                public void execute(Realm realm) {
                                    UserAuthenticationRequest result = realm.where(UserAuthenticationRequest.class).equalTo("id", UserAuthenticationRequest.KEY).findFirst();
                                    result.setAccessToken(authenticationReceipt.accessToken);
                                }
                            });

                            backendService.getUser(authenticationReceipt.userId, new GroupsCallback<User, BackendError>() {
                                @Override
                                public void onSuccess(User user) {
                                    final Resources resources = getResources();
                                    final Uri uri = new Uri.Builder()
                                            .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                                            .authority(resources.getResourcePackageName(R.drawable.default_profile_pic))
                                            .appendPath(resources.getResourceTypeName(R.drawable.default_profile_pic))
                                            .appendPath(resources.getResourceEntryName(R.drawable.default_profile_pic))
                                            .build();
                                    UserProfile userProfile = userOnboardingService.onBoardUser(user, uri.toString(), new UserColorPalette(0, 0, 0, 0));
                                    GroupsApp.getContext().setCurrentUserProfile(userProfile);
                                    Intent i = new Intent(getActivity(), UserActivity.class);
                                    Bundle b = new Bundle();
                                    b.putString("userId", user.id);
                                    i.putExtras(b);
                                    startActivity(i);
                                }

                                @Override
                                public void onFailure(BackendError backendError) {
                                    Logger.e("ERROR REGISTERING USER! " + backendError.getMessage());
                                }
                            });
                        }

                        @Override
                        public void onFailure(BackendError backendError) {
                            Logger.d("Failed to authenticate user. %s", backendError.getMessage());
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void handleLoggedInState() {
        backendService.isAuthenticated(new GroupsCallback<AuthenticationReceipt, BackendError>() {
            @Override
            public void onSuccess(final AuthenticationReceipt authenticationReceipt) {
//                startActivity(new Intent(GroupsApp.getContext(), UserActivity.class));
                Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {

                    @Override
                    public void execute(Realm realm) {
                        UserAuthenticationRequest result = realm.where(UserAuthenticationRequest.class).equalTo("id", UserAuthenticationRequest.KEY).findFirst();
                        result.setAccessToken(authenticationReceipt.accessToken);
                    }
                });
                onBoardAndStartActivity(authenticationReceipt);
            }

            @Override
            public void onFailure(BackendError backendError) {
                Logger.d("Failed to authenticate user. %s", backendError.getMessage());
            }
        });
    }

    private void onBoardAndStartActivity(AuthenticationReceipt authenticationReceipt) {
        backendService.getUser(authenticationReceipt.userId, new GroupsCallback<User, BackendError>() {
            @Override
            public void onSuccess(User user) {
                final Resources resources = getResources();
                final Uri uri = new Uri.Builder()
                        .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                        .authority(resources.getResourcePackageName(R.drawable.default_profile_pic))
                        .appendPath(resources.getResourceTypeName(R.drawable.default_profile_pic))
                        .appendPath(resources.getResourceEntryName(R.drawable.default_profile_pic))
                        .build();
                UserProfile userProfile = userOnboardingService.onBoardUser(user, uri.toString(), new UserColorPalette(0, 0, 0, 0));
                GroupsApp.getContext().setCurrentUserProfile(userProfile);
                Intent i = new Intent(getActivity(), UserActivity.class);
                Bundle b = new Bundle();
                b.putString("userId", user.id);
                i.putExtras(b);
                startActivity(i);
            }

            @Override
            public void onFailure(BackendError backendError) {
                Logger.e("ERROR REGISTERING USER! " + backendError.getMessage());
            }
        });
    }
}