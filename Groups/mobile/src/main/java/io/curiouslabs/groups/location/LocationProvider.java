package io.curiouslabs.groups.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.common.GroupsCallback;
import io.curiouslabs.groups.common.GroupsError;
import io.curiouslabs.groups.common.Tuple;
import io.curiouslabs.groups.model.dto.BackendError;
import io.curiouslabs.groups.service.PermissionService;

/**
 * Created by visitor15 on 12/12/16.
 */

public class LocationProvider {

    public static final int PERMISSION_REQUEST_CODE = 100;

//    private LocationManager     locationManager;
//    private PermissionService   permissionService;

    public void resolveLocation(final Activity activity, final GroupsCallback<Location, GroupsError> groupsCallback) throws SecurityException {
        PermissionService permissionService = new PermissionService();
        final LocationManager locationManager = (LocationManager) GroupsApp.getContext().getSystemService(Context.LOCATION_SERVICE);
        permissionService.requestPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION, LocationProvider.PERMISSION_REQUEST_CODE, new GroupsCallback<Tuple<Boolean, Integer>, GroupsError>() {
            @Override
            public void onSuccess(Tuple<Boolean, Integer> booleanIntegerTuple) {
                resolveLocationInternal(locationManager, 0, groupsCallback);
            }

            @Override
            public void onFailure(GroupsError groupsError) {
                groupsCallback.onFailure(new BackendError(101, "Location permission request denied"));
            }
        });


    }

    private void resolveLocationInternal(LocationManager locationManager, final long delayInterval, final GroupsCallback<Location, GroupsError> groupsCallback) throws SecurityException {
        locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                groupsCallback.onSuccess(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        }, Looper.myLooper());
    }
}
