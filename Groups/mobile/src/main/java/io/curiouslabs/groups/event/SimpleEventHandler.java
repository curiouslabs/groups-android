package io.curiouslabs.groups.event;

import com.google.common.base.Optional;

import java.util.EmptyStackException;
import java.util.Stack;

/**
 * Created by visitor15 on 12/28/16.
 */

public class SimpleEventHandler extends EventBroker {

    private Stack<Eventable> uiEventStack;
    private Stack<Eventable> dataEventStack;
    private Stack<Eventable> anyEventStack;

    public SimpleEventHandler() {
        this.uiEventStack   = new Stack<>();
        this.dataEventStack = new Stack<>();
        this.anyEventStack  = new Stack<>();
    }

    @Override
    protected void handleUIEvent(Eventable event) {
        uiEventStack.push(event);
    }

    @Override
    protected void handleDataEvent(Eventable event) {
        dataEventStack.push(event);
    }

    @Override
    protected void handleAnyEvent(Eventable event) {
        anyEventStack.push(event);
    }

    public void clearEvents() {
        uiEventStack = new Stack<>();
        dataEventStack = new Stack<>();
        anyEventStack = new Stack<>();
    }

    public Stack<Eventable> getUiEventStack() {
        return uiEventStack;
    }

    public Stack<Eventable> getDataEventStack() {
        return dataEventStack;
    }

    public Stack<Eventable> getAnyEventStack() {
        return anyEventStack;
    }

    public Optional<Eventable> getNextUiEvent() {
        try {
            return Optional.fromNullable(uiEventStack.pop());
        } catch (EmptyStackException e) {
            return Optional.absent();
        }
    }

    public Optional<Eventable> getNextDataEvent() {
        try {
            return Optional.fromNullable(dataEventStack.pop());
        } catch (EmptyStackException e) {
            return Optional.absent();
        }
    }

    public Optional<Eventable> getNextAnyEvent() {
        try {
            return Optional.fromNullable(anyEventStack.pop());
        } catch (EmptyStackException e) {
            return Optional.absent();
        }
    }
}
