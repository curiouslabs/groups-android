package io.curiouslabs.groups.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.activity.SimpleActivity;
import io.curiouslabs.groups.common.GroupsCallback;
import io.curiouslabs.groups.model.domain.AuthenticationReceipt;
import io.curiouslabs.groups.model.domain.User;
import io.curiouslabs.groups.model.dto.AuthenticationRequestDTO;
import io.curiouslabs.groups.model.dto.BackendError;
import io.curiouslabs.groups.model.dto.UserCreateRequestDTO;
import io.curiouslabs.groups.service.BackendService;

/**
 * Created by visitor15 on 11/29/16.
 */

public class RegisterUserFragment extends Fragment {

    @Inject
    BackendService backendService;

    @BindView(R.id.input_email)
    EditText inputEmail;

    @BindView(R.id.input_password)
    EditText inputPassword;

    @BindView(R.id.input_password_retyped)
    EditText inputPasswordRetyped;

    @BindView(R.id.input_firstname)
    EditText inputFirstName;

    @BindView(R.id.input_lastname)
    EditText inputLastName;

    @BindView(R.id.input_opt_displayname)
    EditText inputDisplayName;

    @BindView(R.id.btn_register)
    Button btnRegister;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.register_user_layout, container, false);
        GroupsApp.getContext().getLoginActivityComponent().inject(this);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UserCreateRequestDTO userCreateRequestDTO = buildCreationRequest();
                backendService.createUser(userCreateRequestDTO, new GroupsCallback<User, BackendError>() {
                    @Override
                    public void onSuccess(User user) {
                        backendService.authenticate(new AuthenticationRequestDTO("", userCreateRequestDTO.email, userCreateRequestDTO.password, ""), new GroupsCallback<AuthenticationReceipt, BackendError>() {
                            @Override
                            public void onSuccess(AuthenticationReceipt authenticationReceipt) {
                                Logger.d("Got authentication for user %s. Access token: %s", authenticationReceipt.userId, authenticationReceipt.accessToken);
                                startActivity(new Intent(GroupsApp.getContext(), SimpleActivity.class));
                            }

                            @Override
                            public void onFailure(BackendError backendError) {
                                Logger.d("Failed to authenticate user. %s", backendError.getMessage());
                            }
                        });
                    }

                    @Override
                    public void onFailure(BackendError backendError) {
                        Logger.d("Failed to create user. %s", backendError.getMessage());
                    }
                });
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private UserCreateRequestDTO buildCreationRequest() {
        String email            = inputEmail.getText().toString();
        String password         = inputPassword.getText().toString();
        String reTypedPassword  = inputPasswordRetyped.getText().toString();
        String firstName        = inputFirstName.getText().toString();
        String lastName         = inputLastName.getText().toString();
        String diplayName       = inputDisplayName.getText().toString();

        return new UserCreateRequestDTO(email, password, firstName, lastName, diplayName);
    }
}
