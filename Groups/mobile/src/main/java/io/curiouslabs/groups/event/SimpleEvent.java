package io.curiouslabs.groups.event;

/**
 * Created by visitor15 on 10/18/16.
 */

public class SimpleEvent implements Eventable<SimpleEvent> {

    private final int code;

    private final String message;

    public SimpleEvent(final int code, final String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public EventType getEventType() {
        return EventType.ANY_EVENT;
    }
}
