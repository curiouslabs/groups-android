package io.curiouslabs.groups.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.common.base.Optional;
import com.orhanobut.logger.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.BaseActivity;
import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.gui.RoundedImageView;
import io.curiouslabs.groups.model.domain.UserProfile;

/**
 * Created by visitor15 on 12/11/16.
 */

public class UserActivity extends BaseActivity {

    @BindView(R.id.profile_image)
    RoundedImageView profileImage;

//    @BindView(R.id.content_main_root)
//    RelativeLayout mainView;

//    @BindView(R.id.collapsing_toolbar)
//    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.appbar_layout)
    AppBarLayout appBarLayout;

    @Override
    protected View onSimpleCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return parent;
    }

    @Override
    protected void onSimpleResume() {
//        EventBus.getDefault().register(this);
        Logger.d("WAKKA");
    }

    @Override
    protected void onSimplePause() {
//        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onSimpleStart() {
//        Optional<UserProfile> optUserProfile = GroupsApp.getContext().getCurrentUserProfile();
//        if(optUserProfile.isPresent()) {
//            UserProfile userProfile = optUserProfile.get();
//            String name = (!Strings.isNullOrEmpty(userProfile.getUser().displayName)) ? userProfile.getUser().displayName : userProfile.getUser().firstName;
//            getSupportActionBar().setTitle(name);
//        }
    }

    @Override
    protected void onSimpleOptionsItemSelected(MenuItem item) {
//        Handle action bar item clicks here. The action bar will
//        automatically handle clicks on the Home/Up button, so long
//        as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            // Do stuff
        }
    }

    @Override
    protected void onSimpleCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
    }

    @Override
    protected void onSimpleBackPressed() { }

    @Override
    protected void onSimpleCreate(Bundle savedInstanceState) {
        Optional<UserProfile> userProfile = GroupsApp.getContext().getCurrentUserProfile();
        if(userProfile.isPresent()) {
            String userId = userProfile.get().getUser().id;
            Logger.d("GOT USER ID " + userId);
        }
        else {
            Logger.e("NO USER PROFILE FOUND HERE!!");
        }

    }

    @Override
    protected void bindResources() {
        ButterKnife.bind(this);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void handleEvent(ColorPaletteChangeEvent event) {
//        collapsingToolbarLayout.setBackgroundColor(event.getVibrantColor());
//        collapsingToolbarLayout.setContentScrimColor(event.getVibrantColor());
//    }
}