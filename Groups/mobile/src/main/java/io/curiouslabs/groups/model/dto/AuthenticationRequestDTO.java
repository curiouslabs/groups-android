package io.curiouslabs.groups.model.dto;

/**
 * Created by visitor15 on 11/25/16.
 */

public class AuthenticationRequestDTO extends JsonEntity<AuthenticationRequestDTO>{

    public final String auth;

    public final String email;

    public final String password;

    public final String id;

    public AuthenticationRequestDTO(String auth, String email, String password, String id) {
        this.auth = auth;
        this.email = email;
        this.password = password;
        this.id = id;
    }

    @Override
    protected Class<AuthenticationRequestDTO> getClassInternal() {
        return AuthenticationRequestDTO.class;
    }
}
