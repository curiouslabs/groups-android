package io.curiouslabs.groups.dagger;

import dagger.Component;
import io.curiouslabs.groups.dagger.module.AuthenticatedRequestModule;
import io.curiouslabs.groups.http.AuthenticatedRequestInterceptor;

/**
 * Created by visitor15 on 11/25/16.
 */
@Component(modules = { AuthenticatedRequestModule.class })
public interface AuthenticatedRequestComponent {

    void inject(AuthenticatedRequestInterceptor authenticatedRequestInterceptor);
}
