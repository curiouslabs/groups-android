package io.curiouslabs.groups.dagger.module;

import dagger.Module;
import dagger.Provides;
import io.curiouslabs.groups.gui.DefaultFabOnClickListener;
import io.curiouslabs.groups.gui.DefaultNavDrawerListener;

/**
 * Created by visitor15 on 10/18/16.
 */
@Module
public class BaseActivityModule {

    @Provides
    public static DefaultNavDrawerListener provideNavDrawAdapter() {
        return new DefaultNavDrawerListener();
    }

    @Provides
    public static DefaultFabOnClickListener provideFabOnClickListener() {
        return new DefaultFabOnClickListener();
    }
}
