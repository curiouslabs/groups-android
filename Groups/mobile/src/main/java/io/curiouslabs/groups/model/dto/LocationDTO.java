package io.curiouslabs.groups.model.dto;

/**
 * Created by visitor15 on 11/25/16.
 */

public class LocationDTO {

    public final Double latitude;
    public final Double longitude;

    public LocationDTO(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
