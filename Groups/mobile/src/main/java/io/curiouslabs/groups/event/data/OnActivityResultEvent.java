package io.curiouslabs.groups.event.data;

import android.os.Bundle;

/**
 * Created by visitor15 on 12/30/16.
 */

public class OnActivityResultEvent {

    private final int id;

    private final Bundle bundle;

    public OnActivityResultEvent(final int id, final Bundle bundle) {
        this.id = id;
        this.bundle = bundle;
    }

    public int getId() {
        return this.id;
    }

    public Bundle getData() {
        return this.bundle;
    }
}
