package io.curiouslabs.groups.dagger.module;

import dagger.Module;
import dagger.Provides;
import io.curiouslabs.groups.service.BackendService;

/**
 * Created by visitor15 on 11/25/16.
 */
@Module
public class BackendServiceModule {

    @Provides
    public static BackendService provideBackendService() {
        return new BackendService();
    }
}
