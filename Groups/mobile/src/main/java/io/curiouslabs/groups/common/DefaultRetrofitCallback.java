package io.curiouslabs.groups.common;

import com.orhanobut.logger.Logger;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by visitor15 on 11/25/16.
 */

public class DefaultRetrofitCallback<T, K, E extends GroupsError> implements Callback<T> {

    private final GroupsCallback<K, E> groupsCallback;
    private final GroupsConverter<T, K> groupsConverter;
    private final ErrorFactory<E> errorFactory;

    public DefaultRetrofitCallback(GroupsCallback<K, E> groupsCallback, GroupsConverter<T, K> groupsConverter, ErrorFactory<E> errorFactory) {
        this.groupsCallback     = groupsCallback;
        this.groupsConverter    = groupsConverter;
        this.errorFactory       = errorFactory;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if(response.isSuccessful()) {
            if (response.body() != null) {
                final T t = response.body();
                groupsCallback.onSuccess(groupsConverter.convert(t));
            } else {
                try {
                    String errorMessage = response.errorBody().string();
                    int code = response.code();
                    Logger.d("No entity returned %s", errorMessage);
                    groupsCallback.onFailure(errorFactory.create(code, errorMessage));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            try {
                String errorMessage = response.errorBody().string();
                int code = response.code();
                Logger.d("Error creating entity - %s", errorMessage);
                groupsCallback.onFailure(errorFactory.create(code, errorMessage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Logger.d("Failed to create entity - %s", t.getMessage());
        groupsCallback.onFailure(errorFactory.create(500, t.getMessage()));
    }
}
