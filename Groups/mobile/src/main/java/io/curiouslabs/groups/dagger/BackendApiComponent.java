package io.curiouslabs.groups.dagger;

import dagger.Component;
import io.curiouslabs.groups.dagger.module.BackendApiModule;
import io.curiouslabs.groups.dagger.module.LocationModule;
import io.curiouslabs.groups.dagger.module.PermissionServiceModule;
import io.curiouslabs.groups.service.BackendService;

/**
 * Created by visitor15 on 11/25/16.
 */
@Component(modules = { BackendApiModule.class, PermissionServiceModule.class, LocationModule.class })
public interface BackendApiComponent {

    void inject(BackendService backendService);
}
