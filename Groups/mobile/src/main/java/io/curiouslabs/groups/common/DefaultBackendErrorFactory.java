package io.curiouslabs.groups.common;

import io.curiouslabs.groups.model.dto.BackendError;

/**
 * Created by visitor15 on 11/25/16.
 */

public class DefaultBackendErrorFactory implements ErrorFactory<BackendError> {
    @Override
    public BackendError create(int code, String message) {
        return new BackendError(code, message);
    }
}
