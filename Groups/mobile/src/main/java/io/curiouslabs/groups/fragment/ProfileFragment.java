package io.curiouslabs.groups.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.gui.ColorPalette;
import io.curiouslabs.groups.gui.RoundedImageView;

/**
 * Created by visitor15 on 12/11/16.
 */

public class ProfileFragment extends Fragment {

    @BindView(R.id.profile_image)
    RoundedImageView profileImage;

    @BindView(R.id.header_layout)
    RelativeLayout headerLayout;

    @BindView(R.id.body_layout)
    RelativeLayout bodyLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.profile_layout, container, false);
//        GroupsApp.getContext().getLoginActivityComponent().inject(this);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        Glide.with(this).load(R.drawable.mount_everest).into(profileImage);
        ColorPalette.getColorPaletteAsync(profileImage.getDrawingCache(), new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                headerLayout.setBackgroundColor(palette.getDarkMutedSwatch().getRgb());
                bodyLayout.setBackgroundColor(palette.getLightMutedSwatch().getRgb());
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
