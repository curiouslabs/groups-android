package io.curiouslabs.groups.event.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by visitor15 on 1/3/17.
 */

public class ShowFragmentRequestEvent {

    private final Class<? extends Fragment> clazz;
    private final Bundle data;

    public ShowFragmentRequestEvent(final Class<? extends Fragment> clazz, final Bundle data) {
        this.clazz = clazz;
        this.data = data;
    }

    public Bundle getData() {
        return this.data;
    }

    public Class<? extends Fragment> getFragmentClass() {
        return this.clazz;
    }
}
