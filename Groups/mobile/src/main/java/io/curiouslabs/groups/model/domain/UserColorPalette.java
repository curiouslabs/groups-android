package io.curiouslabs.groups.model.domain;

/**
 * Created by visitor15 on 12/31/16.
 */

public class UserColorPalette {

    private final int vibrantColor;
    private final int darkVibrantColor;
    private final int mutedVibrant;
    private final int mutedDarkVibrant;

    public UserColorPalette(int vibrantColor, int darkVibrantColor, int mutedVibrant, int mutedDarkVibrant) {
        this.vibrantColor = vibrantColor;
        this.darkVibrantColor = darkVibrantColor;
        this.mutedVibrant = mutedVibrant;
        this.mutedDarkVibrant = mutedDarkVibrant;
    }

    public int getVibrantColor() {
        return vibrantColor;
    }

    public int getDarkVibrantColor() {
        return darkVibrantColor;
    }

    public int getMutedVibrant() {
        return mutedVibrant;
    }

    public int getMutedDarkVibrant() {
        return mutedDarkVibrant;
    }
}
