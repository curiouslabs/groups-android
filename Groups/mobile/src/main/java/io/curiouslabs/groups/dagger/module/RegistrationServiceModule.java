package io.curiouslabs.groups.dagger.module;

import dagger.Module;
import dagger.Provides;
import io.curiouslabs.groups.service.RegistrationService;

/**
 * Created by visitor15 on 1/2/17.
 */
@Module
public class RegistrationServiceModule {

    @Provides
    public static RegistrationService provideRegistrationService() {
        return new RegistrationService();
    }
}
