package io.curiouslabs.groups.event;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by visitor15 on 12/28/16.
 */

public abstract class EventBroker {

    @Inject
    @Named("defaultBus")
    EventBus eventBus;

    public EventBroker() {
        init();
    }

    private void init() {
        eventBus.register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(Eventable event) {
        switch(event.getEventType()) {
            case UI_EVENT:
                handleUIEvent(event);
                break;
            case DATA_EVENT:
                handleDataEvent(event);
                break;
            case ANY_EVENT:
                handleAnyEvent(event);
                break;
        }
    }

    protected abstract void handleUIEvent(Eventable event);

    protected abstract void handleDataEvent(Eventable event);

    protected abstract void handleAnyEvent(Eventable event);
}
