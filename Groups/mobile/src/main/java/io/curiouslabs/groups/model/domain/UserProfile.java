package io.curiouslabs.groups.model.domain;

/**
 * Created by visitor15 on 12/31/16.
 */

public class UserProfile {

    private User user;

    private String profilePicUri;

    private UserColorPalette userColorPalette;

    public UserProfile(User user, String profilePicUri, UserColorPalette userColorPalette) {
        this.user = user;
        this.profilePicUri = profilePicUri;
        this.userColorPalette = userColorPalette;
    }

    public User getUser() {
        return user;
    }

    public String getProfilePicUri() {
        return profilePicUri;
    }

    public UserColorPalette getUserColorPalette() {
        return userColorPalette;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserColorPalette(UserColorPalette userColorPalette) {
        this.userColorPalette = userColorPalette;
    }

    public void setProfilePicUri(String profilePicUri) {
        this.profilePicUri = profilePicUri;
    }
}
