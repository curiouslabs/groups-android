package io.curiouslabs.groups.event.data;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.curiouslabs.groups.event.EventListener;

/**
 * Created by visitor15 on 12/29/16.
 */

public class LocationChangedEventListener {

    private final EventListener<LocationChangedEvent> eventListener;

    public LocationChangedEventListener(EventListener<LocationChangedEvent> eventListener) {
        this.eventListener = eventListener;
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(LocationChangedEvent event) {
        this.eventListener.onEvent(event);
    }
}
