package io.curiouslabs.groups;

import android.support.multidex.MultiDexApplication;

import com.google.common.base.Optional;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import io.curiouslabs.groups.dagger.AuthenticatedRequestComponent;
import io.curiouslabs.groups.dagger.BackendApiComponent;
import io.curiouslabs.groups.dagger.BackendServiceComponent;
import io.curiouslabs.groups.dagger.BaseActivityComponent;
import io.curiouslabs.groups.dagger.DaggerAuthenticatedRequestComponent;
import io.curiouslabs.groups.dagger.DaggerBackendApiComponent;
import io.curiouslabs.groups.dagger.DaggerBackendServiceComponent;
import io.curiouslabs.groups.dagger.DaggerBaseActivityComponent;
import io.curiouslabs.groups.dagger.DaggerLoginActivityComponent;
import io.curiouslabs.groups.dagger.LoginActivityComponent;
import io.curiouslabs.groups.model.domain.UserProfile;
import io.realm.Realm;

/**
 * Created by visitor15 on 11/24/16.
 */

public class GroupsApp extends MultiDexApplication {

    private static GroupsApp singleton;

    private static RefWatcher refWatcher;

    private static BaseActivityComponent baseActivityComponent;

    private static AuthenticatedRequestComponent authenticatedRequestComponent;

    private static BackendApiComponent backendApiComponent;

    private static BackendServiceComponent backendServiceComponent;

    private static LoginActivityComponent loginActivityComponent;

    private static UserProfile userProfile;

    @Override
    public void onCreate() {
        GroupsApp.singleton = this;
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        init();
        super.onCreate();
    }

    public static GroupsApp getContext() {
        return GroupsApp.singleton;
    }

    public static RefWatcher getRefWatcher() {
        return GroupsApp.singleton.refWatcher;
    }

    private void init() {
        refWatcher                      = LeakCanary.install(GroupsApp.singleton);
        baseActivityComponent           = DaggerBaseActivityComponent.builder().build();
        authenticatedRequestComponent   = DaggerAuthenticatedRequestComponent.builder().build();
        backendApiComponent             = DaggerBackendApiComponent.builder().build();
        backendServiceComponent         = DaggerBackendServiceComponent.builder().build();
        loginActivityComponent          = DaggerLoginActivityComponent.builder().build();
        Realm.init(this);
    }

    public BaseActivityComponent getSimpleActivityComponent() {
        return baseActivityComponent;
    }

    public AuthenticatedRequestComponent getAuthenticatedRequestComponent() {
        return authenticatedRequestComponent;
    }

    public BackendApiComponent getBackendApiComponent() {
        return backendApiComponent;
    }

    public BackendServiceComponent getBackendServiceComponent() {
        return backendServiceComponent;
    }

    public LoginActivityComponent getLoginActivityComponent() {
        return loginActivityComponent;
    }

    public Optional<UserProfile> getCurrentUserProfile() {
        return Optional.fromNullable(this.userProfile);
    }

    public void setCurrentUserProfile(final UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}