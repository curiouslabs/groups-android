package io.curiouslabs.groups.dagger;

import dagger.Component;
import io.curiouslabs.groups.BaseActivity;
import io.curiouslabs.groups.dagger.module.BackendServiceModule;
import io.curiouslabs.groups.dagger.module.BaseActivityModule;
import io.curiouslabs.groups.dagger.module.BaseEventModule;

/**
 * Created by visitor15 on 10/18/16.
 */
@Component(modules = { BaseActivityModule.class, BaseEventModule.class, BackendServiceModule.class})
public interface BaseActivityComponent {

    void inject(BaseActivity baseActivity);
}
