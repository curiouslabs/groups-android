package io.curiouslabs.groups.service;

import com.google.common.base.Optional;

import javax.inject.Inject;

import io.curiouslabs.groups.model.domain.User;
import io.curiouslabs.groups.model.domain.UserColorPalette;
import io.curiouslabs.groups.model.domain.UserProfile;

/**
 * Created by visitor15 on 12/31/16.
 */

public class RegistrationService {

    @Inject
    UserProfileStorageService userProfileStorageService;

    public RegistrationService() {
        this.userProfileStorageService = new UserProfileStorageService();
    }

    public UserProfile registerUser(final User user, final String profilePicUri, final UserColorPalette userColorPalette) {
        Optional<UserProfile> optUserProfile = userProfileStorageService.findUserProfileById(user.id);
        if(optUserProfile.isPresent()) {
            // TODO update?
            return optUserProfile.get();
        }
        else {
            // New user
            UserProfile userProfile = createUserProfile(user, profilePicUri, userColorPalette);
            userProfileStorageService.saveUserProfile(userProfile);
            return userProfile;
        }
    }

    private UserProfile createUserProfile(final User user, final String profilePicUri, final UserColorPalette userColorPalette) {
        return new UserProfile(user, profilePicUri, userColorPalette);
    }
}
