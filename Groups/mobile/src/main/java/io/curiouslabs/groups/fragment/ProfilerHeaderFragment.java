package io.curiouslabs.groups.fragment;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.BaseActivity;
import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.event.data.ColorPaletteChangeEvent;
import io.curiouslabs.groups.event.data.OnActivityResultEvent;
import io.curiouslabs.groups.gui.ColorPalette;
import io.curiouslabs.groups.gui.RoundedImageView;
import io.curiouslabs.groups.model.domain.UserColorPalette;
import io.curiouslabs.groups.model.domain.UserProfile;
import io.curiouslabs.groups.service.UserProfileStorageService;

/**
 * Created by visitor15 on 12/30/16.
 */

public class ProfilerHeaderFragment extends Fragment {

    @BindView(R.id.profiler_header_container)
    View profileHeaderContainer;

    @BindView(R.id.profile_image)
    RoundedImageView profileImage;

    @BindView(R.id.profile_image_container)
    FrameLayout profileImageContainer;

    private UserProfileStorageService userProfileStorageService = new UserProfileStorageService();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_header_layout, container, false);
        ButterKnife.bind(this, v);
//        ((BaseActivity) getActivity()).initializeToolbar(toolbar);
        v.post(new Runnable() {
            @Override
            public void run() {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    // get the center for the clipping circle
                    int cx = (profileHeaderContainer.getLeft() + profileHeaderContainer.getRight()) / 2;
                    int cy = (profileHeaderContainer.getTop() + profileHeaderContainer.getBottom()) / 2;

                    // get the final radius for the clipping circle
                    int dx = Math.max(cx, profileHeaderContainer.getWidth() - cx);
                    int dy = Math.max(cy, profileHeaderContainer.getHeight() - cy);
                    float finalRadius = (float) Math.hypot(dx, dy);

                    // Android native animator
                    Animator animator = ViewAnimationUtils.createCircularReveal(profileHeaderContainer, cx, cy, 0, finalRadius);
                    animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    animator.setDuration(500);
                    animator.start();
                }
            }
        });

//        profileHeaderContainer.post(new Runnable() {
//            @Override
//            public void run() {
//                Logger.d("RUNNNNNNING!");
//                TransitionManager.beginDelayedTransition(profileImageContainer,
//                        new ChangeBounds().setPathMotion(new ArcMotion()).setStartDelay(500).setDuration(1000));
//
//                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) profileImage.getLayoutParams();
//                params.gravity = (Gravity.LEFT | Gravity.TOP);
//                profileImage.setLayoutParams(params);
//            }
//        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 101);
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d("HIT RESUME!");
        EventBus.getDefault().register(this);
        final Optional<UserProfile> optUserProfile = GroupsApp.getContext().getCurrentUserProfile();
        if(optUserProfile.isPresent()) {
            final UserProfile userProfile = optUserProfile.get();
            String name = (!Strings.isNullOrEmpty(userProfile.getUser().displayName)) ? userProfile.getUser().displayName : userProfile.getUser().firstName;
            ((BaseActivity) getActivity()).getSupportActionBar().setTitle(name);
            Glide.with(this).load(userProfile.getProfilePicUri()).asBitmap().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    profileImage.setImageBitmap(resource);
                    profileHeaderContainer.setBackgroundColor(userProfile.getUserColorPalette().getVibrantColor());
                }
            });
        }
        else {
            Logger.e("NO USER PROFILE FOUND!");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(OnActivityResultEvent event) {
        switch(event.getId()) {
            case 101: {
                handleNewProfilePic("");
            }
            default: {
                Logger.e("COULD NOT MATCH AGAINST EVENT ID");
                handleNewProfilePic(event.getData().getString("uri"));
            }
        }
    }

    private void handleNewProfilePic(final String uri) {
        Glide.with(this).load(uri).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                profileImage.setImageBitmap(resource);
                ColorPalette.getColorPaletteAsync(resource, new Palette.PaletteAsyncListener() {
                    @Override
                    public void onGenerated(Palette palette) {
                        int vibrantColor = palette.getVibrantColor(getResources().getColor(R.color.colorPrimary));
                        int darkVibrantColor = palette.getDarkVibrantColor(palette.getDarkVibrantColor(getResources().getColor(R.color.colorPrimaryDark)));
                        int darkMutedColor = palette.getDarkMutedColor(palette.getDarkVibrantColor(getResources().getColor(R.color.colorPrimaryDark)));
                        int lightMutedColor = palette.getLightMutedColor(getResources().getColor(R.color.colorPrimary));

                        UserColorPalette userColorPalette = new UserColorPalette(vibrantColor, darkVibrantColor, lightMutedColor, darkMutedColor);
                        profileHeaderContainer.setBackgroundColor(palette.getVibrantColor(getResources().getColor(R.color.colorPrimary)));
                        Optional<UserProfile> optUserProfile = GroupsApp.getContext().getCurrentUserProfile();
                        if(optUserProfile.isPresent()) {
                            UserProfile userProfile = optUserProfile.get();
                            userProfile.setProfilePicUri(uri);
                            userProfile.setUserColorPalette(userColorPalette);
                            userProfileStorageService.saveUserProfile(userProfile);
                        }
                        EventBus.getDefault().post(new ColorPaletteChangeEvent(vibrantColor, darkVibrantColor));
                    }
                });
            }
        });
    }
}
