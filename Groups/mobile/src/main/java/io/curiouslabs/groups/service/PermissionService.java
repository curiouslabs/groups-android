package io.curiouslabs.groups.service;

import android.app.Activity;
import android.support.v4.app.ActivityCompat;

import com.orhanobut.logger.Logger;

import io.curiouslabs.groups.common.GroupsCallback;
import io.curiouslabs.groups.common.GroupsError;
import io.curiouslabs.groups.common.Tuple;
import io.curiouslabs.groups.event.EventListener;
import io.curiouslabs.groups.event.PermissionGrantedEvent;
import io.curiouslabs.groups.event.PermissionGrantedEventListener;

/**
 * Created by visitor15 on 12/29/16.
 */

public class PermissionService {

    private PermissionGrantedEventListener permissionGrantedEventListener;

    private GroupsCallback<Tuple<Boolean, Integer>, GroupsError> groupsCallback;

    public PermissionService() { }

    private void init() {
        this.permissionGrantedEventListener = new PermissionGrantedEventListener(new EventListener<PermissionGrantedEvent>() {
            @Override
            public void onEvent(PermissionGrantedEvent event) {
                Logger.d("GOT PermissionGrantedEvent EVENT!");
                groupsCallback.onSuccess(new Tuple<Boolean, Integer>(true, event.getPermissionRequestCode()));
                permissionGrantedEventListener.unregisterEventListener();
                groupsCallback = null;
            }
        });
    }

    public void requestPermission(Activity activity, String permission, int permissionRequestCode, final GroupsCallback<Tuple<Boolean, Integer>, GroupsError> groupsCallback) {
        this.groupsCallback = groupsCallback;
        init();

        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            Logger.d("SHOULD SHOW EXPLANATION FOR PERMISSION REQUEST. WE WILL JUST REQUEST THE PERMISSION AGAIN FOR NOW.");
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

            ActivityCompat.requestPermissions(activity,
                    new String[]{permission},
                    permissionRequestCode);

        } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(activity,
                    new String[]{permission},
                    permissionRequestCode);

            // PERMISSION_REQUEST_CODE is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }
}
