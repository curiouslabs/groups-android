package io.curiouslabs.groups.dagger.module;

import dagger.Module;
import dagger.Provides;
import io.curiouslabs.groups.dagger.provider.AuthProvider;
import io.curiouslabs.groups.dagger.provider.BackendAuthProvider;

/**
 * Created by visitor15 on 11/25/16.
 */
@Module
public class AuthenticatedRequestModule {

    @Provides
    public static AuthProvider provideAuthProvider() {
        return BackendAuthProvider.getInstance();
    }
}
