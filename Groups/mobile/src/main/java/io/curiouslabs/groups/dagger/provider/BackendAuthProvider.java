package io.curiouslabs.groups.dagger.provider;

import android.util.Base64;

import com.orhanobut.logger.Logger;

import io.curiouslabs.groups.model.domain.UserAuthenticationRequest;
import io.realm.Realm;

/**
 * Created by visitor15 on 11/25/16.
 */

public class BackendAuthProvider implements AuthProvider {

    private static BackendAuthProvider instance;

    private String token;

    private BackendAuthProvider() {
        instance = this;
    }

    public static BackendAuthProvider getInstance() {
        if(instance == null) {
            new BackendAuthProvider();
        }
        return instance;
    }

    @Override
    public String getToken() {
        Realm instance = Realm.getDefaultInstance();
        UserAuthenticationRequest result = instance.where(UserAuthenticationRequest.class).equalTo("id", UserAuthenticationRequest.KEY).findFirst();
        String accessToken = null;
        if(result != null) {
            accessToken = result.getAccessToken();
        }
        else {
            Logger.e("No authorization found in " + getClass().getSimpleName());
        }
        instance.close();



        return accessToken;
    }

    @Override
    public void setToken(final String newToken) {
        this.token = new String(Base64.encodeToString(newToken.getBytes(), Base64.URL_SAFE));
    }
}
