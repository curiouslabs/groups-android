package io.curiouslabs.groups.event;

/**
 * Created by visitor15 on 12/28/16.
 */

public enum EventType {
    UI_EVENT,
    DATA_EVENT,
    ANY_EVENT
}
