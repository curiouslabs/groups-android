package io.curiouslabs.groups.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Optional;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.BaseActivity;
import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.event.data.ColorPaletteChangeEvent;
import io.curiouslabs.groups.model.domain.UserProfile;

/**
 * Created by visitor15 on 1/8/17.
 */

public class UserHomeFragment extends Fragment {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    private ProfilerHeaderFragment  profilerHeaderFragment;
    private NearByGroupsFragment    nearByGroupsFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_home_fragment_layout, container, false);
        ButterKnife.bind(this, v);

        profilerHeaderFragment  = (ProfilerHeaderFragment) getChildFragmentManager().findFragmentById(R.id.profile_header_fragment);
        nearByGroupsFragment    = (NearByGroupsFragment) getChildFragmentManager().findFragmentById(R.id.nearby_groups_fragment);

        Optional<UserProfile> optUserProfile = GroupsApp.getContext().getCurrentUserProfile();
        if(optUserProfile.isPresent()) {
            int bgColor = optUserProfile.get().getUserColorPalette().getVibrantColor();
            collapsingToolbarLayout.setBackgroundColor(bgColor);
            collapsingToolbarLayout.setContentScrimColor(bgColor);
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BaseActivity) getActivity()).initializeToolbar(toolbar);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(ColorPaletteChangeEvent event) {
        collapsingToolbarLayout.setBackgroundColor(event.getVibrantColor());
        collapsingToolbarLayout.setContentScrimColor(event.getVibrantColor());
    }
}
