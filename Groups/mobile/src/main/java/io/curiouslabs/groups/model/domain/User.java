package io.curiouslabs.groups.model.domain;

import io.curiouslabs.groups.model.dto.UserDTO;

/**
 * Created by visitor15 on 11/24/16.
 */

public class User {

    public final String id;
    public final String email;
    public final String firstName;
    public final String lastName;
    public final String displayName;

    public User(String id, String email, String firstName, String lastName, String displayName) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.displayName = displayName;
    }

    public static User fromDTO(final UserDTO userDTO) {
        return new User(userDTO.id, userDTO.email, userDTO.firstName, userDTO.lastName, userDTO.displayName);
    }
}
