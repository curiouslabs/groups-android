package io.curiouslabs.groups.model.persistant;

import io.curiouslabs.groups.model.domain.User;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by visitor15 on 12/31/16.
 */

public class PersistedUser extends RealmObject {

    @PrimaryKey
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String displayName;

    public PersistedUser() {

    }

    public PersistedUser(final User user) {
        this(user.id, user.email, user.firstName, user.lastName, user.displayName);
    }

    public PersistedUser(String id, String email, String firstName, String lastName, String displayName) {
        this.id             = id;
        this.email          = email;
        this.firstName      = firstName;
        this.lastName       = lastName;
        this.displayName    = displayName;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static User toDomainModel(final PersistedUser persistedUser) {
        return new User(persistedUser.id, persistedUser.email, persistedUser.firstName, persistedUser.lastName, persistedUser.displayName);
    }
}
