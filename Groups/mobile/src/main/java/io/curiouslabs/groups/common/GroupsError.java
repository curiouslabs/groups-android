package io.curiouslabs.groups.common;

/**
 * Created by visitor15 on 11/25/16.
 */

public interface GroupsError {
    public int getCode();

    public String getMessage();
}
