package io.curiouslabs.groups.common;

/**
 * Created by visitor15 on 11/25/16.
 */

public interface GroupsConverter<T, K> {

    public K convert(T t);
}
