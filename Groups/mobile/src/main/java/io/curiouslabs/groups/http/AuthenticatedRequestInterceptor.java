package io.curiouslabs.groups.http;

import com.orhanobut.logger.Logger;

import java.io.IOException;

import javax.inject.Inject;

import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.dagger.provider.AuthProvider;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by visitor15 on 11/25/16.
 */

public class AuthenticatedRequestInterceptor implements Interceptor {

    @Inject
    AuthProvider authProvider;

    public AuthenticatedRequestInterceptor() {
        GroupsApp.getContext().getAuthenticatedRequestComponent().inject(this);
    }

    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();

//        authProvider.setToken("default_access_token");
        String authToken = authProvider.getToken();
        Logger.d("Appending access token to request: %s", authToken);

        request = request.newBuilder().addHeader("Authorization", "Bearer " + authToken).build();

        Response response = chain.proceed(request);

        return response;
    }
}