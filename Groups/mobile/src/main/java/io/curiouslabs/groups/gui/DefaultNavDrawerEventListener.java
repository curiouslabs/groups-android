package io.curiouslabs.groups.gui;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.event.ui.NavDrawerEvent;

/**
 * Created by visitor15 on 10/23/16.
 */

public class DefaultNavDrawerEventListener {

    private final DrawerLayout drawerLayout;

    public DefaultNavDrawerEventListener(final DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSimpleEvent(NavDrawerEvent event) {
        this.drawerLayout.closeDrawer(GravityCompat.START, true);
        Toast.makeText(GroupsApp.getContext(), event.getAction().name(), Toast.LENGTH_SHORT).show();
    }
}
