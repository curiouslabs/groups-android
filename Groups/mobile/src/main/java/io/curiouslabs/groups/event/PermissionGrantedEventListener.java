package io.curiouslabs.groups.event;

import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by visitor15 on 12/29/16.
 */

public class PermissionGrantedEventListener {

    private final EventListener<PermissionGrantedEvent> eventListener;

    public PermissionGrantedEventListener(EventListener<PermissionGrantedEvent> eventListener) {
        this.eventListener = eventListener;
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(PermissionGrantedEvent event) {
        Logger.d("GOT PermissionGrantedEvent!");
        this.eventListener.onEvent(event);
    }

    public void unregisterEventListener() {
        EventBus.getDefault().unregister(this);
    }
}
