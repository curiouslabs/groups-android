package io.curiouslabs.groups.gui;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by visitor15 on 1/2/17.
 */

public class RecyclerViewGridSpacer extends RecyclerView.ItemDecoration {

    private final int horizontalSpaceHeight;
    private final int verticalSpaceHeight;
    private final int spanCount;

    private final boolean includeEdge;

    public RecyclerViewGridSpacer(int horizontalSpaceHeight, int verticalSpaceHeight, int spanCount, boolean includeEdge) {
        this.horizontalSpaceHeight  = horizontalSpaceHeight;
        this.verticalSpaceHeight    = verticalSpaceHeight;
        this.spanCount              = spanCount;
        this.includeEdge            = includeEdge;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column = position % spanCount; // item column

        if (includeEdge) {
            outRect.left = horizontalSpaceHeight - column * horizontalSpaceHeight / spanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * horizontalSpaceHeight / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

            if (position < spanCount) { // top edge
                outRect.top = verticalSpaceHeight;
            }
            outRect.bottom = verticalSpaceHeight; // item bottom
        } else {
            outRect.left = column * horizontalSpaceHeight / spanCount; // column * ((1f / spanCount) * spacing)
            outRect.right = horizontalSpaceHeight - (column + 1) * horizontalSpaceHeight / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) {
                outRect.top = verticalSpaceHeight; // item top
            }
        }
    }
}
