package io.curiouslabs.groups.model.dto;


/**
 * Created by visitor15 on 11/25/16.
 */

public class UserCreateRequestDTO extends JsonEntity<UserCreateRequestDTO> {

    public final String email;
    public final String password;
    public final String firstName;
    public final String lastName;
    public final String displayName;

    public UserCreateRequestDTO(final String email,
                                final String password,
                                final String firstName,
                                final String lastName,
                                final String displayName) {
        this.email          = email;
        this.password       = password;
        this.firstName      = firstName;
        this.lastName       = lastName;
        this.displayName    = displayName;
    }

    @Override
    protected Class<UserCreateRequestDTO> getClassInternal() {
        return UserCreateRequestDTO.class;
    }
}
