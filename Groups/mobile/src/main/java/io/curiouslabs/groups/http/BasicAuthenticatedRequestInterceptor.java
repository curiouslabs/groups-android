package io.curiouslabs.groups.http;

import android.util.Base64;

import com.orhanobut.logger.Logger;

import java.io.IOException;

import io.curiouslabs.groups.model.domain.UserAuthenticationRequest;
import io.realm.Realm;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by visitor15 on 11/25/16.
 */

public class BasicAuthenticatedRequestInterceptor  implements Interceptor {



    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Realm instance = Realm.getDefaultInstance();
        UserAuthenticationRequest result = instance.where(UserAuthenticationRequest.class).equalTo("id", UserAuthenticationRequest.KEY).findFirst();
        if(result != null) {
            Logger.d("Got email for auth: %s ", result.getEmail());
            String basicAuthStr = Base64.encodeToString(new String(result.getEmail() + ":" + result.getPassword()).getBytes(), Base64.URL_SAFE).trim();
            Logger.d("Appending basic auth to request: %s", basicAuthStr);
            request = request.newBuilder().addHeader("Authorization", "Basic " + basicAuthStr).build();
        }
        else {
            Logger.e("No authorization found in " + getClass().getSimpleName());
        }
        instance.close();

        Response response = chain.proceed(request);
        return response;
    }
}