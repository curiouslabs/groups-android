package io.curiouslabs.groups.service;

import android.app.Activity;
import android.location.Location;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.curiouslabs.groups.GroupsApp;
import io.curiouslabs.groups.common.DefaultBackendErrorFactory;
import io.curiouslabs.groups.common.DefaultRetrofitCallback;
import io.curiouslabs.groups.common.GroupsCallback;
import io.curiouslabs.groups.common.GroupsConverter;
import io.curiouslabs.groups.common.GroupsError;
import io.curiouslabs.groups.http.api.Backend;
import io.curiouslabs.groups.location.LocationProvider;
import io.curiouslabs.groups.model.domain.AuthenticationReceipt;
import io.curiouslabs.groups.model.domain.Group;
import io.curiouslabs.groups.model.domain.User;
import io.curiouslabs.groups.model.domain.UserAuthenticationRequest;
import io.curiouslabs.groups.model.dto.AuthenticationReceiptDTO;
import io.curiouslabs.groups.model.dto.AuthenticationRequestDTO;
import io.curiouslabs.groups.model.dto.BackendError;
import io.curiouslabs.groups.model.dto.GroupDTO;
import io.curiouslabs.groups.model.dto.UserCreateRequestDTO;
import io.curiouslabs.groups.model.dto.UserDTO;
import io.realm.Realm;

/**
 * Created by visitor15 on 11/25/16.
 */

public class BackendService {

    @Inject @Named("authenticatedBackend")
    Backend authenticatedBackend;

    @Inject @Named("basicAuthenticatedBackend")
    Backend basicAuthenticatedBackend;

    @Inject
    Backend unauthenticatedBackend;

    @Inject
    LocationProvider locationProvider;

    public BackendService() {
        GroupsApp.getContext().getBackendApiComponent().inject(this);
    }

    public void isAuthenticated(final GroupsCallback<AuthenticationReceipt, BackendError> groupsCallback) {
        basicAuthenticatedBackend.authenticateUser(new AuthenticationRequestDTO("", "", "", "")).enqueue(
                new DefaultRetrofitCallback<AuthenticationReceiptDTO, AuthenticationReceipt, BackendError>(groupsCallback, new GroupsConverter<AuthenticationReceiptDTO, AuthenticationReceipt>() {
                    @Override
                    public AuthenticationReceipt convert(AuthenticationReceiptDTO authenticationReceiptDTO) {
                        return AuthenticationReceipt.fromDTO(authenticationReceiptDTO);
                    }
                }, new DefaultBackendErrorFactory()));
    }

    public void authenticate(final AuthenticationRequestDTO authenticationRequestDTO, final GroupsCallback<AuthenticationReceipt, BackendError> groupsCallback) {
        Realm instance = Realm.getDefaultInstance();
        instance.beginTransaction();
        UserAuthenticationRequest userAuthenticationRequest = new UserAuthenticationRequest(UserAuthenticationRequest.KEY, authenticationRequestDTO.email, authenticationRequestDTO.password, "", "");
        instance.copyToRealmOrUpdate(userAuthenticationRequest);
        instance.commitTransaction();
        instance.close();
        basicAuthenticatedBackend.authenticateUser(authenticationRequestDTO).enqueue(
                new DefaultRetrofitCallback<AuthenticationReceiptDTO, AuthenticationReceipt, BackendError>(groupsCallback, new GroupsConverter<AuthenticationReceiptDTO, AuthenticationReceipt>() {
            @Override
            public AuthenticationReceipt convert(AuthenticationReceiptDTO authenticationReceiptDTO) {
                return AuthenticationReceipt.fromDTO(authenticationReceiptDTO);
            }
        }, new DefaultBackendErrorFactory()));
    }

    public void createUser(final UserCreateRequestDTO userCreateRequestDTO, final GroupsCallback<User, BackendError> groupsCallback) {
        unauthenticatedBackend.createUser(userCreateRequestDTO).enqueue(
                new DefaultRetrofitCallback<UserDTO, User, BackendError>(groupsCallback, new GroupsConverter<UserDTO, User>() {
            @Override
            public User convert(UserDTO userDTO) {
                return User.fromDTO(userDTO);
            }
        }, new DefaultBackendErrorFactory()));
    }

    public void getUser(final String id, final GroupsCallback<User, BackendError> groupsCallback) {
        Logger.d("GETTING USER FOR ID: " + id);
        authenticatedBackend.getUser(id).enqueue(
                new DefaultRetrofitCallback<UserDTO, User, BackendError>(groupsCallback, new GroupsConverter<UserDTO, User>() {
            @Override
            public User convert(UserDTO userDTO) {
                return User.fromDTO(userDTO);
            }
        }, new DefaultBackendErrorFactory()));
    }

    public void getGroup(final String id, final GroupsCallback<Group, BackendError> groupsCallback) {
        authenticatedBackend.getGroup(id).enqueue(
                new DefaultRetrofitCallback<GroupDTO, Group, BackendError>(groupsCallback, new GroupsConverter<GroupDTO, Group>() {
            @Override
            public Group convert(GroupDTO groupDTO) {
                return Group.fromDTO(groupDTO);
            }
        }, new DefaultBackendErrorFactory()));
    }

    public void getNearByGroups(final Activity activity, final GroupsCallback<List<Group>, BackendError> groupsCallback) {
        locationProvider.resolveLocation(activity, new GroupsCallback<Location, GroupsError>() {
            @Override
            public void onSuccess(Location location) {
                Logger.d("GOT LOCATION " + location.getLatitude() + ", " + location.getLongitude());
                authenticatedBackend.searchGroups(location.getLatitude(), location.getLongitude()).enqueue(new DefaultRetrofitCallback<List<GroupDTO>, List<Group>, BackendError>(groupsCallback, new GroupsConverter<List<GroupDTO>, List<Group>>() {
                    @Override
                    public List<Group> convert(List<GroupDTO> groupDTOs) {
                        List<Group> groups = new ArrayList<Group>(groupDTOs.size());
                        for(GroupDTO groupDTO : groupDTOs) {
                            groups.add(Group.fromDTO(groupDTO));
                        }
                        return groups;
                    }
                }, new DefaultBackendErrorFactory()));
            }

            @Override
            public void onFailure(GroupsError groupsError) {
                groupsCallback.onFailure(new BackendError(groupsError.getCode(), groupsError.getMessage()));
            }
        });
    }
}
