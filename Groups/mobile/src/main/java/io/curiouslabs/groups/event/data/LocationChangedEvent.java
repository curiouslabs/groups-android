package io.curiouslabs.groups.event.data;

import android.location.Location;

import io.curiouslabs.groups.event.EventType;
import io.curiouslabs.groups.event.Eventable;

/**
 * Created by visitor15 on 12/29/16.
 */

public class LocationChangedEvent implements Eventable<LocationChangedEvent> {

    private final Location location;

    public LocationChangedEvent(final Location location) {
        this.location = location;
    }

    @Override
    public EventType getEventType() {
        return EventType.DATA_EVENT;
    }

    public Location getLocation() {
        return this.location;
    }
}
