package io.curiouslabs.groups.dagger.module;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.curiouslabs.groups.http.api.Backend;
import io.curiouslabs.groups.http.builder.BackendBuilder;

/**
 * Created by visitor15 on 11/25/16.
 */
@Module
public class BackendApiModule {

    @Provides
    public static Backend provideBackend() {
        return BackendBuilder.getInstance().buildUnauthenticatedBackend();
    }

    @Provides @Named(value = "authenticatedBackend")
    public static Backend providedAuthenticatedBackend() {
        return BackendBuilder.getInstance().buildAuthenticatedBackend();
    }

    @Provides @Named(value = "basicAuthenticatedBackend")
    public static Backend providedBasicAuthenticatedBackend() {
        return BackendBuilder.getInstance().buildBasicAuthenticatedBackend();
    }
}
