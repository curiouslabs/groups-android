package io.curiouslabs.groups.event;

/**
 * Created by visitor15 on 12/29/16.
 */

public interface EventListener<T> {

    public void onEvent(T event);
}
