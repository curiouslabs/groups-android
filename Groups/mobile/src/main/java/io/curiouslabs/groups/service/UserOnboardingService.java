package io.curiouslabs.groups.service;

import javax.inject.Inject;

import io.curiouslabs.groups.model.domain.User;
import io.curiouslabs.groups.model.domain.UserColorPalette;
import io.curiouslabs.groups.model.domain.UserProfile;

/**
 * Created by visitor15 on 1/2/17.
 */

public class UserOnboardingService {

    @Inject
    RegistrationService registrationService;

    public UserOnboardingService() {
        this.registrationService = new RegistrationService();
    }

    public UserProfile onBoardUser(final User user, final String profilePicUri, final UserColorPalette userColorPalette) {
        return registrationService.registerUser(user, profilePicUri, userColorPalette);
    }
}
