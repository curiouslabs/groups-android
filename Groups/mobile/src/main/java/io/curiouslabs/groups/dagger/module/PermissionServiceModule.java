package io.curiouslabs.groups.dagger.module;

import dagger.Module;
import dagger.Provides;
import io.curiouslabs.groups.service.PermissionService;

/**
 * Created by visitor15 on 12/29/16.
 */
@Module
public class PermissionServiceModule {

    @Provides
    public static PermissionService providePermissionService() {
        return new PermissionService();
    }
}
