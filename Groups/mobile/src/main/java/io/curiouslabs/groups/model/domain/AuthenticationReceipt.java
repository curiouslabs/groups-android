package io.curiouslabs.groups.model.domain;

import io.curiouslabs.groups.model.dto.AuthenticationReceiptDTO;

/**
 * Created by visitor15 on 11/25/16.
 */

public class AuthenticationReceipt {

    public final String userId;
    public final String accessToken;
    public final String tokenType;
    public final Long   expiresIn;
    public final String refreshToken;

    public AuthenticationReceipt(String userId,
                                 String accessToken,
                                 String tokenType,
                                 Long expiresIn,
                                 String refreshToken) {
        this.userId         = userId;
        this.accessToken    = accessToken;
        this.tokenType      = tokenType;
        this.expiresIn      = expiresIn;
        this.refreshToken   = refreshToken;
    }

    public static AuthenticationReceipt fromDTO(AuthenticationReceiptDTO authenticationReceiptDTO) {
        return new AuthenticationReceipt(authenticationReceiptDTO.userId,
                authenticationReceiptDTO.accessToken,
                authenticationReceiptDTO.tokenType,
                authenticationReceiptDTO.expiresIn,
                authenticationReceiptDTO.refreshToken);
    }
}
