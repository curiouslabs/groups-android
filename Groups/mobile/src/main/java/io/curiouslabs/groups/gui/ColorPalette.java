package io.curiouslabs.groups.gui;

import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;

/**
 * Created by visitor15 on 12/11/16.
 */

public class ColorPalette {

    public static void getColorPaletteAsync(final Bitmap bitmap, final Palette.PaletteAsyncListener paletteAsyncListener) {
        Palette.from(bitmap).generate(paletteAsyncListener);
    }
}
