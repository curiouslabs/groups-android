package io.curiouslabs.groups.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.curiouslabs.groups.R;
import io.curiouslabs.groups.model.domain.Group;

/**
 * Created by visitor15 on 1/3/17.
 */

public class GroupInformationFragment extends Fragment {

    @BindView(R.id.sliding_tabs)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.group_information_fragment_layout, container, false);
        ButterKnife.bind(this, v);

//        viewPager.setAdapter(new DetailedGroupFragmentPagerAdapter(getActivity().getSupportFragmentManager(), getActivity()));
//        tabLayout.setupWithViewPager(viewPager);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
//        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void setGroup(final Group group) {
        viewPager.setAdapter(new DetailedGroupFragmentPagerAdapter(getActivity().getSupportFragmentManager(), getActivity(), group));
        tabLayout.setupWithViewPager(viewPager);
    }

    public class DetailedGroupFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 3;
        private String tabTitles[] = new String[] { "About", "Activity", "Members" };
        private Context context;
        private Group group;

        public DetailedGroupFragmentPagerAdapter(FragmentManager fm, Context context, Group group) {
            super(fm);
            this.context = context;
            this.group = group;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(position + 1, group);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }

    public static class PageFragment extends Fragment {
        public static final String ARG_PAGE = "ARG_PAGE";

        private int mPage;

        public static PageFragment newInstance(int page, Group group) {

            switch(page) {
                case 1: {
                    Bundle args = new Bundle();
                    args.putString(AboutPageFragment.ARG_DESCRIPTION, group.description);
                    AboutPageFragment aboutPageFragment = new AboutPageFragment();
                    aboutPageFragment.setArguments(args);
                    return aboutPageFragment;
                }
                default: {
                    Bundle args = new Bundle();
                    args.putInt(ARG_PAGE, page);
                    PageFragment fragment = new PageFragment();
                    fragment.setArguments(args);
                    return fragment;
                }
//                case 2: {
//                    Bundle args = new Bundle();
//                    args.putInt(ARG_PAGE, page);
//                    PageFragment fragment = new PageFragment();
//                    fragment.setArguments(args);
//                    return fragment;
//                }
//                case 3: {
//                    Bundle args = new Bundle();
//                    args.putInt(ARG_PAGE, page);
//                    PageFragment fragment = new PageFragment();
//                    fragment.setArguments(args);
//                    return fragment;
//                }
            }


        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mPage = getArguments().getInt(ARG_PAGE);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.detailed_group_item_layout, container, false);
            TextView textView = (TextView) view;
            textView.setText("Fragment #" + mPage);
            return view;
        }
    }

    public static class AboutPageFragment extends PageFragment {
        public static final String ARG_DESCRIPTION = "arg_description";

        private String description;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            description = getArguments().getString(ARG_DESCRIPTION);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.detailed_group_item_layout, container, false);
            TextView textView = (TextView) view;
            textView.setText(description);
            return view;
        }
    }
}